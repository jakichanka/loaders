import React, { useEffect, useRef, useState } from "react";
import { gsap } from "gsap";

export const Info = () => {
  const el = useRef<any>();

  const [reversed, setReversed] = useState(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const timeout = (ms: number) => {
    return new Promise((resolve) => setTimeout(resolve, ms));
  };

  useEffect(() => {
    (async () => {
      // setIsLoading(true);
      // await timeout(2000)
      // setIsLoading(false)
      // ref.current = gsap.timeline().to(ref.current, {transform: 'rotate(45deg)'}).to(ref.current, {left: '50%', transform: 'translateX(-50%)', background: 'yellow'})
      gsap.registerEffect({
        name: "fade",
        effect: (targets: any, config: any) => {
          return gsap.to(targets, {duration: config.duration, opacity: 0});
        },
        defaults: {duration: 2}, //defaults get applied to any "config" object passed to the effect
        extendTimeline: true, //now you can call the effect directly on any GSAP timeline to have the result immediately inserted in the position you define (default is sequenced at the end)
      });

      el.current = gsap
        .timeline()
        .fade(el.current, {duration: 3}).fade({duration: 1}, "+=2").to(el.current, {x:100})
    })();
  }, []);

  useEffect(() => {
    el.current.reversed(reversed);
  }, [reversed]);

  return (
    <>
      {!isLoading && (
        <>
          <div
            className="box"
            style={{
              width: "200px",
              left: 0,
              height: "200px",
              background: 'url(https://www.svgrepo.com/show/343410/cool-emoticon-emotion-expression-face-smiley-sunglasses.svg) no-repeat ' ,
              backgroundSize: 'cover',
              position: "absolute",
              borderRadius: "10px",
            }}
            ref={el}
          >sadasdsadsadsadsadsa </div>
          <button
            style={{ marginTop: "220px" }}
            onClick={() => setReversed(!reversed)}
          >
            Reverse
          </button>
        </>
      )}
    </>
  );
};
