import "./style.scss";

export const TransfOri = () => {
  return (
    <div
      style={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        height: "100vh",
      }}
    >
      <div className='parent' style={{ width: 200, height: 200, position: 'relative', borderRadius: '50%', overflow:"hidden" }}>
        <div className='block animateLeft'/>
        <div className='block animateRight'/>
          <div className='block animateTop'/>
          <div className='block animateBottom'/>
      </div>
    </div>
  );
};
